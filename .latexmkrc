# -*- cperl -*-
# latexmkrc
$builddir = $ENV{'BUILDDIR'};
$depsfile = $ENV{'DEPSFILE'};
$bibdirs  = $ENV{'BIBDIRS'};
$viewer   = $ENV{'VIEWER'};

$recorder = 1;
$pdf_mode = 1;
$bibtex_use = 2;
$force_mode = 1;
$dependents_list = 1;
$silence_logfile_warnings = 0; # FIXME: not working proberly, use LaTeX ackage silence
$use_make_for_missing_files = 1;
$out_dir = $builddir;
$deps_file = "$out_dir/$depsfile.deps";
@BIBINPUTS = ('.',$bibdirs);

$pdflatex = 'pdflatex %O -interaction=nonstopmode -synctex=1 -file-line-error %S';
$pdf_previewer = $viewer;

push @generated_exts, 'lol', 'sta', 'synctex.gz';