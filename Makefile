include .utils

BASENAME     = demo
BUILDDIR     = build

# environment
TEXDIRS      = resources/tex:resources/latex-sty:resources/listings:resources/latex
TEXDIRS     := $(TEXDIRS):resources/images:resources/tables:resources/latex
TEXDIRS     := $(TEXDIRS):resources/figures
BIBDIRS      = resources/latex/
BSTDIR       = resources/latex-bst/

# cf. latexmkrc
LATEXMK      = latexmk $(OPTS)$1 | fold -sw $(out_length)
VIEWER       = xdg-open

all: build

build: setup
	$(call LATEXMK_COND, )

log: setup
	$(call LATEXMK_ERRS, $(TARGET))

view: setup
	$(call LATEXMK_COND, -pv)

preview: setup
	$(call LATEXMK_COND, -pvc)

clean: setup
	$(call LATEXMK_COND, -c)

distclean: setup
	$(call LATEXMK_COND, -C)

help:
	@echo $(INSTRUCTIONS)

%/:
	mkdir -p $@

setup: .FORCE | $(BUILDDIR)/

.PHONY: all clean distclean .FORCE

# export all variables by default
.EXPORT_ALL_VARIABLES:
TEXINPUTS := :.:$(TEXDIRS)
# bibtex invoked from BUILDDIR, thus prepend './../'
BSTINPUTS := ./../$(BSTDIR)
BIBINPUTS := :.:./../$(BIBDIRS)

# disable implicit suffix and built-in rules (for performance and profit)
.SUFFIXES:
MAKEFLAGS += --no-builtin-rules